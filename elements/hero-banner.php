<section class="hero-banner-wrappper position-relative" style="background: url(<?php echo  get_the_post_thumbnail_url()?>)">
    <div class="container hero-banner-inner">

        <div class="hero-banner-title-wrapper">
            <h1 class="text-white"><?php echo get_the_title()?></h1>
            <?php
            if (function_exists('yoast_breadcrumb')) {
                yoast_breadcrumb('<p id="breadcrumbs" class="color-1"> ', '</p>');
            }
            ?>
        </div>


    </div>
</section>