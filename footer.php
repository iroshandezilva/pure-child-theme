<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
?>

<?php get_template_part('sidebar-templates/sidebar', 'footerfull'); ?>

<div class="bg-dark-h" id="wrapper-footer">


</div><!-- wrapper end -->
<footer class="site-footer  bg-dark-h">
    <div class="container text-white py-3">
        <p class="text-white mb-0">2018 © Client, All rights reserved.Design & Developed By <a href="https://www.iroshandezilva.com/" class="color-1 mb-0 ">Iroshan De Zilva</a> </p>
    </div>

</footer><!-- #colophon -->
</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

