
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );



/// ******  Upload SVG0   ****/

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');




/// ******  Enque Script  ******  ///


function styles_scripts(){
    {
        wp_enqueue_script("jquery_js", 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js', array("jquery"));
        wp_enqueue_script("custom_js", get_stylesheet_directory_uri() . "css/custom.js", array("jquery_js") );
        wp_localize_script('custom_js', 'the_ajax_script', array('ajaxurl' => admin_url('admin-ajax.php')));
    }
}

function child_theme_stylesheets() {
    $base = get_stylesheet_directory_uri();
    wp_enqueue_style( 'child-theme-style', get_stylesheet_directory_uri()."css/custom.css" );
    return;
}
add_action( 'wp_enqueue_scripts', 'child_theme_stylesheets', 18 );



/// ****** Include Banner  Pages  ******  ///

add_action("get_banner_php","get_banner_php_home");

function get_banner_php_home(){
    include 'elements/hero-banner.php';
}



/// ****** WP Media short code  ******  ///

function img_shortcode($atts)
{
    // Attributes
    $atts = shortcode_atts(
        array(
        'id' => '',
		'size'=>'thumbnail'
        ), $atts, 'my_image'
    );

    $img= wp_get_attachment_image_src($atts['id'], $atts['size']);
    $image_alt = get_post_meta($atts['id'], '_wp_attachment_image_alt', TRUE);

    $return = '';
    $return = '<img src="' . $img[0] . '" alt="' .$image_alt .'" />';

    // Return HTML code
    return $return;
}

add_shortcode('my_image', 'img_shortcode');

//Use this shortcode [my_image id='' size='medium'] to inlude image


//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
	$existing_mimes['webp'] = 'image/webp';
	return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');


//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
	if ($result === false) {
		$displayable_image_types = array( IMAGETYPE_WEBP );
		$info = @getimagesize( $path );

		if (empty($info)) {
			$result = false;
		} elseif (!in_array($info[2], $displayable_image_types)) {
			$result = false;
		} else {
			$result = true;
		}
	}

	return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);
